import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ViewerPageRoutingModule } from './viewer-routing.module';
import { ViewerPage } from './viewer.page';


import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

@NgModule({
  imports: [
    NgxExtendedPdfViewerModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ViewerPageRoutingModule,
  ],
  declarations: [ViewerPage]
})
export class ViewerPageModule { }
