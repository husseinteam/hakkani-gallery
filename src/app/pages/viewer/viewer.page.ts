import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from 'src/services/rest.service';
import { UriContainer, Uri } from 'src/models/uri';
import { Platform } from '@ionic/angular';
import { NgxExtendedPdfViewerComponent } from "ngx-extended-pdf-viewer";

declare var jQuery: any;


@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.page.html',
  styleUrls: ['./viewer.page.scss'],
})
export class ViewerPage implements OnInit {
  @ViewChild('hakkaniPdfViewer', { static: true }) hakkaniPdfViewer: NgxExtendedPdfViewerComponent;
  constructor(private rest: RestService, public platform: Platform) {
  }
  uri: Uri = null;
  isBrowser: boolean;
  pdfNumber: number = 0;
  ngOnInit() {
    if (!this.platform.is('cordova')) {
      this.isBrowser = true;
      this.rest.getPdfUriContainer()
        .subscribe((data: UriContainer) => {
          this.uri = data.uris[this.pdfNumber];
          this.customizeFlipBook();
        });
    } else {
      this.isBrowser = false;
      this.rest.getPdfUriContainer()
        .subscribe((data: UriContainer) => {
          this.uri = data.uris[this.pdfNumber];
          this.hakkaniPdfViewer.src = this.uri.url;
        });
    }

  }
  customizeFlipBook() {
    var book = (<any>jQuery('.flip-book-container')).FlipBook({
      pdf: this.uri.url,
      scale: 2.5,
    });
  }

}
