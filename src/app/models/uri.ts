export interface Uri {
    title: string;
    url: string;
}
export interface UriContainer {
    uris: Uri[];
}
