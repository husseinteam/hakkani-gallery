import { Injectable } from '@angular/core';

const Cryptr = require('cryptr');

@Injectable({
  providedIn: 'root'
})
export class EnvService {
  CRYPTR: any;
  constructor() { this.CRYPTR = new Cryptr('myT331441otalySecretKey'); }
}
