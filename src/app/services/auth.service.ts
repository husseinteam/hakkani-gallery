import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
import { User } from "src/app/models/user";
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  constructor(private storage: NativeStorage,
    private env: EnvService,
  ) { }
  register(userName: string, password: string): Promise<string> {
    return new Promise<string>((next) => this.storage.getItem(userName).then(credentials => {
      if (credentials) {
        next(`Hoşgeldiniz ${(credentials as String).split("!", 2)[0]}`);
      } else {
        let credentials = this.env.CRYPTR.encrypt(`${userName}!${password}`);
        this.storage.setItem(userName, credentials)
          .then(
            () => {
              console.log('Credentials Stored');
              next(`Hoşgeldiniz ${userName}`);
            },
            error => console.error('Error Storing Credentials', error)
          );
      }
    }));
  }

  login(userName: string, password: string): Promise<boolean> {
    return new Promise<boolean>((next) => this.storage.getItem(userName).then(data => {
      let decrypted = this.env.CRYPTR.decrypt(data);
      let credentials = (decrypted as String).split("!", 2);
      this.storage.setItem("user", data)
        .then(
          () => next(credentials[0] == userName && credentials[1] == password),
          error => console.error('Error Storing Credentials', error)
        );

    }));
  }

  user(): Promise<User> {
    return new Promise<User>((next) => {
      if (this.isLoggedIn) {
        this.storage.getItem("user").then(data => {
          let decrypted = this.env.CRYPTR.decrypt(data);
          let credentials = (decrypted as String).split("!", 2);
          next(new User(credentials[0], credentials[1]));
        });
      }
    });
  }

}
